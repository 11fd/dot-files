function lsfd --description "Personal ls configuration"
    if test (pwd) = ~ || contains ~ $argv
        ls -gvGI'[A-Z_]*' -I'lynx_bookmarks.html' --group-directories-first --si --time-style=iso $argv
    else
        ls -gvGI'_*' --group-directories-first --si --time-style=iso $argv
    end
end
