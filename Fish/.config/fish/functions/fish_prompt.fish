function fish_prompt --description 'Write out the prompt'
    set -l last_status $status
    set -l suffix
    set -l git_branch " g:"(git symbolic-ref --short HEAD 2> /dev/null)
    set -l pijul_branch " p:"(cat .pijul/current_branch 2> /dev/null) # Doesn't work in subdir
    set -l fossil_branch " f:"(fossil branch current 2> /dev/null)
    tty | grep -qF "tty" && set -l p '→' || set -l p '❱'
    switch "$USER"
        case root toor
            set suffix "#$p"
        case '*'
            set suffix "\$$p"
    end

    echo -n -s \
    (test $last_status -eq 0 && set_color red || set_color red --bold) $last_status' '  \
    (set_color cyan) (prompt_pwd) \
    (set_color green) $git_branch $pijul_branch $fossil_branch \
    (set_color normal) " $suffix "

end
