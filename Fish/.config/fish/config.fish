#!/bin/fish
# See /etc/rc.conf for default font and other settings

set -x ALSAPCM 'hw'
set -x ALTERNATE_EDITOR ''
set -x ASPELL_CONF 'lang en_GB; keyboard dvorak'
set -x EDITOR 'eclient'
set -x GROFF_FONT_PATH '/usr/share/groff/site-font/'
set -x HUNT '11fd'
set -x LESS 'Meis'
set -x MORE 'dps'
set -x MOST_SWITCHES '-s'
set -x PAGER 'less'
# set -x PAGER 'more'
# set -x PAGER 'most'
# set -x PAGER 'slit'
# set -x PAGER 'w3m -o con=f -s'
set -x REFER '/home/fd/.refer-database'
set -x SUDO_EDITOR '/bin/nano'
set -x SUDO_PROMPT 'Passwd [%U]: '
# set -x SXHKD_SHELL '/bin/sh'
# set -x TERM 'screen-256color'
set -x TERM 'xterm-256color'
set -x XDG_CONFIG_HOME '/home/fd/.config/'
# set -ax fish_user_paths '/home/fd/.local/bin'
# set -a fish_user_paths '/opt/texlive/2020/bin/x86_64-linux'
set -ax PATH '/home/fd/.local/bin'
set -ax PATH '/opt/texlive/2020/bin/x86_64-linux'

bind --user \em beginning-of-line # M-m
bind --user \eo __fish_paginate   # M-o
bind --user \ep up-or-search      # M-p
bind --user \en down-or-search    # M-n
bind --user \ee forward-bigword   # M-a
bind --user \ea backward-bigword  # M-e

alias la "ls -Ahl"

# man sv | col -b | nano +1,1 -Rvx -   # Nano as pager
