#!/bin/bash

if [[ $- != *i* ]] ; then
	return
fi

PS1='\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '

alias ec='emacsclient -t' 
alias no='nano'
