# 11FD's Configs

Some messy configuration files for various programs. I prune out old configurations when I stop using that program in an effort to keep this concise. And yet somehow, it's still disorganised.

## System info

I run Void Linux on a ThinkPad X260, and a several configurations are tailored to one or both of those facts. I don't expect them to be particularly robust.

## How to get

1. `$ loadkeys /usr/share/kbd/keymaps/i386/dvorak/dvorak.map.gz`
2. `$ git clone https://gitlab.com/11fd/dot-files.git ~/DotStow`
3. Probably run the `install-packages` script.
4. Run the `stow-all` script.

## Extras

* *11fd.map.gz*: Keymap for virtual console. It's Dvorak with a couple small changes.
