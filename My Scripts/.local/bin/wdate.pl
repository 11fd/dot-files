#!/usr/bin/env perl
use strict;
use warnings;

my @ltime = localtime;
my $year = $ltime[5] + 1900;
my $day = $ltime[7] + 1;
my @mdays = (31, 30, 30, 31, 30, 30, 31, 30, 30, 31, 30, 31);
$mdays[5] = 31 if ($year % 4 == 0 && $year % 100 != 0) || $year % 400 == 0;
my $month = 0;
while (0 < $day) {
    $day -= $mdays[$month];
    $month++;
}
$day += $mdays[$month - 1];
if ($day == 31 && ($month == 6 || $month == 12)) {
    printf "%d-%02d-World", $year, $month
} else {
    printf "%d-%02d-%02d%s", $year, $month, $day,
        ('Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat')
        [($day + (0, 3, 5)[$month % 3 - 1]) % 7 - 1];
}
