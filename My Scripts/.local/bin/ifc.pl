#!/usr/bin/env perl
use strict;
use warnings;

my @ltime = localtime;
my $year = $ltime[5] + 1900;
my $day = $ltime[7] + 1;
my @mdays = (28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 29);
$mdays[5] = 29 if ($year % 4 == 0 && $year % 100 != 0) || $year % 400 == 0;
my $month = 0;
while (0 < $day) {
    $day -= $mdays[$month];
    $month++;
}
$day += $mdays[$month - 1];
if (29 == $day) {
    printf "%d-%02d-29:%s", $year, $month, ('LpD', 'YrD')[$month / 6 - 1];
} else {
    printf "%d-%02d-%02d:%s", $year, $month, $day,
        ('di', 'lu', 'ma', 'me', 'ĵa', 've', 'sa')[$day % 7 - 1];
}
