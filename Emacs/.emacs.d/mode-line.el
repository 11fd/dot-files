(make-face 'mode-line-modalka-on)
(set-face-attribute 'mode-line-modalka-on nil
                    :inherit 'mode-line
                    :background "red"
                    :weight 'bold)

(make-face 'mode-line-modalka-off)
(set-face-attribute 'mode-line-modalka-off nil
                    :inherit 'mode-line
                    :background "green"
                    :weight 'bold)

(make-face 'mode-line-parinfer-paren)
(set-face-attribute 'mode-line-parinfer-paren nil
                    :inherit 'mode-line
                    :background "yellow"
                    :weight 'bold)

(make-face 'mode-line-parinfer-indent)
(set-face-attribute 'mode-line-parinfer-indent nil
                    :inherit 'mode-line
                    :background "blue"
                    :weight 'bold)

(defvar active-window (frame-selected-window)
  "The active window within a frame.")

(defun set-active-window ()
  "Set the variable `selected-window' appropriately."
  (when (not (minibuffer-window-active-p (frame-selected-window)))
    (setq active-window (frame-selected-window))
    (force-mode-line-update)))

;; (defun unset-active-window ()
;;   "Unset the variable `selected-window' and update the mode line."
;;   (setq active-window nil)
;;   (force-mode-line-update))

(add-hook 'window-configuration-change-hook #'set-active-window)
;; (add-hook 'focus-in-hook #'set-active-window)
;; (add-hook 'focus-out-hook #'unset-active-window)
(add-hook 'buffer-list-update-hook #'set-active-window)

(defun window-active-p ()
  "Return whether the current window is active."
  (eq active-window (selected-window)))

(setq-default mode-line-format
              '("%e%@ "
                (:eval (let* ((active (window-active-p))
                              (on (if active 'mode-line-modalka-on 'mode-line-inactive))
                              (off (if active 'mode-line-modalka-off 'mode-line-inactive))
                              (par (if active 'mode-line-parinfer-paren 'mode-line-inactive))
                              (ind (if active 'mode-line-parinfer-indent 'mode-line-inactive)))
                         (list
                          (if modalka-mode
                              (propertize " MOVE " 'face on)
                            (propertize " TYPE " 'face off))

                          " %[%+%*%] "        ; Read-only & modification indicators

                          (if parinfer-mode (cond ((eq 'indent parinfer--mode)
                                                   (propertize " (I) " 'face ind))
                                                  ((eq 'paren parinfer--mode)
                                                   (propertize " (P) " 'face par))) "-"))))

                ;; Old method, doesn't change in inactive windows.
                ;; (:eval (if modalka-mode
                ;;            (propertize "ON>" 'face 'mode-line-com-face)
                ;;          (propertize "OFF" 'face 'mode-line-ins-face)))
                ;; "%+%*"
                ;; (:eval (cond ((eq 'paren parinfer--mode)
                ;;               (propertize "(P)" 'face 'mode-line-par-face))
                ;;              ((eq 'indent parinfer--mode)
                ;;               (propertize "(I)" 'face 'mode-line-ind-face))))

                " L%l:C%C:%I - "       ; Line:Column:Size
                (:propertize "%b" face mode-line-buffer-id)
                " - "
                (:propertize mode-name face mode-line-emphasis)
                (:propertize minor-mode-alist)
                "%n %-"))               ; Narrowing indicator and dashes
