;; -*- eval: (allout-mode 1); -*-
;;;_* Package archives
(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
(package-initialize)

;;;_* Load mode-line config
(load (concat user-emacs-directory "mode-line"))

;;;_* Global minor-modes
;;;_ + Enable
;; (electric-pair-mode 1)
(global-auto-revert-mode t)
;; (global-prettify-symbols-mode 1)
(icomplete-mode 1)
(ido-mode 1)
(save-place-mode 1)
(show-paren-mode 1)
(winner-mode 1)

;;;_ + Disable
(gpm-mouse-mode -1)
(menu-bar-mode -1)
(tooltip-mode -1)
(when (fboundp #'tool-bar-mode)
  (tool-bar-mode -1))
(when (fboundp #'scroll-bar-mode)
  (scroll-bar-mode -1))

;;;_* Global hooks
(add-hook 'before-save-hook #'delete-trailing-whitespace)
(add-hook 'prog-mode-hook #'electric-pair-local-mode)
;; (add-hook 'prog-mode-hook #'allout-mode)
(add-hook 'text-mode-hook #'visual-line-mode)

;;;_* Default settings
(setq-default
;;;_ + Personal
 user-mail-address "fdriver@posteo.net"
 user-full-name "Finn Driver"

;;;_ + On
 enable-recursive-minibuffers t
 inhibit-startup-screen t
 ido-enable-flex-matching t
 load-prefer-newer t

;;;_ + Off
 create-lockfiles nil
 disabled-command-function nil
 initial-scratch-message nil
 indent-tabs-mode nil
 ring-bell-function nil
 sentence-end-double-space nil
 ;; visible-cursor nil                     ; xterm no-blink cursor.

;;;_ + Misc
 backward-delete-char-untabify-method 'hungry
 custom-file (locate-user-emacs-file "custom.el")
 dired-listing-switches "-Gagv --si --time-style=iso"
 ediff-window-setup-function 'ediff-setup-windows-plain
 fill-column 96
 ispell-extra-args '("--sug-mode=ultra" "--lang=en_US" "--keyboard=dvorak")
 tab-always-indent 'complete

;;;_ + Save-place
 save-place-file (locate-user-emacs-file "places")
 save-place-forget-unreadable-files nil

;;;_ + Auto-save
 auto-save-interval 200
 auto-save-timeout 20

;;;_ + Auto-backup
 backup-by-copying t
 backup-directory-alist `(("." . ,(locate-user-emacs-file "backups")))
 delete-old-versions t
 kept-new-versions 20
 kept-old-versions 2
 vc-make-backup-files t
 version-control t)

;;;_ + yes/no -> y/n
(defalias 'yes-or-no-p 'y-or-n-p)

;;;_* Keyboard
;;;_ + Swap C-x and C-t
(global-set-key (kbd "C-t") ctl-x-map)
(global-set-key (kbd "C-x") #'transpose-chars)
(define-key ctl-x-map (kbd "C-t") #'exchange-point-and-mark)
(define-key ctl-x-map (kbd "C-x") #'transpose-lines)
(require 'iso-transl)
(define-key key-translation-map (kbd "C-t 8") iso-transl-ctl-x-8-map)

;;;_  - Old method
;; (keyboard-translate ?\C-t ?\C-x)
;; (keyboard-translate ?\C-x ?\C-t)
;; (add-hook 'after-make-frame-functions
;;           (lambda (frame)
;;             (select-frame frame)
;;             (keyboard-translate ?\C-t ?\C-x)
;;             (keyboard-translate ?\C-x ?\C-t)))

;;;_ + IComplete
(define-key icomplete-minibuffer-map (kbd "RET") #'icomplete-force-complete-and-exit)
(define-key icomplete-minibuffer-map (kbd "<left>") #'icomplete-backward-completions)
(define-key icomplete-minibuffer-map (kbd "<right>") #'icomplete-forward-completions)
(define-key icomplete-minibuffer-map (kbd "C-r") #'icomplete-forward-completions)
(define-key icomplete-minibuffer-map (kbd "C-s") #'icomplete-forward-completions)

;;;_ + Query-replace
(define-key query-replace-map (kbd "a") #'automatic)
(define-key query-replace-map (kbd "p") #'backup)

;;;_ + Global
(define-key ctl-x-map (kbd "C-b") #'bs-show)
(global-set-key (kbd "M-/") #'hippie-expand)
(global-set-key (kbd "M-n") #'forward-paragraph)
(global-set-key (kbd "M-o") #'other-window)
(global-set-key (kbd "M-p") #'backward-paragraph)
(global-set-key (kbd "M-r") #'query-replace)
(global-set-key (kbd "M-t") #'execute-extended-command)
(global-set-key (kbd "M-x") #'transpose-words)
(global-set-key (kbd "M-z") #'zap-up-to-char)

;;;_ + Custom paragraph-narrowing
(defun narrow-to-maybe-next-paragraph ()
  "Narrows to current paragraph if narrowing
not already present, else the next paragraph"
  (interactive)
  (when (buffer-narrowed-p)
    (widen)
    (forward-paragraph))
  (save-excursion
    (mark-paragraph)
    (narrow-to-region (point) (mark))
    (deactivate-mark)))
(global-set-key (kbd "<f8>") #'narrow-to-maybe-next-paragraph)
(define-key narrow-map (kbd "t") #'narrow-to-maybe-next-paragraph)

(defun narrow-to-maybe-prev-paragraph ()
  "Narrows to current paragraph if narrowing
not already present, else the prior paragraph"
  (interactive)
  (when (buffer-narrowed-p)
    (backward-paragraph)
    (widen)
    (backward-paragraph))
  (save-excursion
    (mark-paragraph)
    (narrow-to-region (point) (mark))
    (deactivate-mark)))
(global-set-key (kbd "<f5>") #'narrow-to-maybe-prev-paragraph)
(define-key narrow-map (kbd "c") #'narrow-to-maybe-prev-paragraph)

;;;_* Use-Package
(when (not (package-installed-p 'use-package))
  (package-refresh-contents)
  (package-install 'use-package))
(require 'use-package)

;;;_ + Delight - Adjust mode-line lighters
(use-package delight
  :ensure t
  :config
  (delight '((abbrev-mode " Abv" abbrev)
             (allout-mode " 📤" allout)
             (calc-mode "Clc" :major)
             (calculator-mode "¹²³" :major)
             (debugger-mode "🐛" :major)
             (eldoc-mode nil eldoc)
             (emacs-lisp-mode "EL" :major)
             (fundamental-mode "ᚠ" :major)
             (lisp-interaction-mode "LI" :major)
             (nroff-electric-mode nil nroff-mode)
             (overwrite-mode " ↹" simple)
             (sh-mode "Sh" :major)
             (text-mode "Txt" :major)
             (visual-line-mode " ↲" simple))))

;;;_ + Rainbow-delimiters - Colourful parenthesis matching
(use-package rainbow-delimiters
  :ensure t
  :init
  (add-hook 'prog-mode-hook #'rainbow-delimiters-mode))

;;;_ + Which-key - Prompt possible keybindings
(use-package which-key
  :ensure t
  :delight
  :config
  (which-key-mode t))

;;;_ + Modalka - Simple modal keybinding for Emacs
(use-package modalka
  :ensure t
  :delight
  :config
  (setq modalka-excluded-modes #'(bs-mode
                                  calc-mode
                                  calculator-mode
                                  debugger-mode
                                  dired-mode
                                  ibuffer-mode
                                  ses-mode))
  (global-set-key (kbd "<end>") #'modalka-mode)
  (global-set-key (kbd "<select>") #'modalka-mode) ; Console
  (modalka-define-kbd "D" "M-d")
  (modalka-define-kbd "H" "M-DEL")
  (modalka-define-kbd "Q" "C-q")
  (modalka-define-kbd "W" "M-w")
  (modalka-define-kbd "Y" "M-y")
  (modalka-define-kbd "a" "C-a")
  (modalka-define-kbd "b" "C-b")
  (modalka-define-kbd "c" "M-p")
  (modalka-define-kbd "d" "C-d")
  (modalka-define-kbd "e" "C-e")
  (modalka-define-kbd "f" "C-f")
  (modalka-define-kbd "g" "C-g")
  (modalka-define-kbd "h" "DEL")
  (modalka-define-kbd "i" "C-i")
  (modalka-define-kbd "j" "C-j")
  (modalka-define-kbd "k" "C-k")
  (modalka-define-kbd "l" "C-l")
  (modalka-define-kbd "m" "M-m")
  (modalka-define-kbd "n" "C-n")
  (modalka-define-kbd "o" "C-o")
  (modalka-define-kbd "p" "C-p")
  (modalka-define-kbd "q" "C-t C-c")
  (modalka-define-kbd "r" "C-r")
  (modalka-define-kbd "s" "C-s")
  (modalka-define-kbd "t" "M-n")
  (modalka-define-kbd "u" "<end>")
  (modalka-define-kbd "v" "C-v")
  (modalka-define-kbd "w" "C-w")
  (modalka-define-kbd "x" "C-t C-e")
  (modalka-define-kbd "y" "C-y")
  (modalka-define-kbd "z" "C-t s")
  (modalka-define-kbd "0" "C-t )")
  (modalka-define-kbd "1" "C-t 1")
  (modalka-define-kbd "2" "C-t 2")
  (modalka-define-kbd "3" "C-t 3")
  (modalka-define-kbd "4" "C-t C-f")
  (modalka-define-kbd "5" "M-%")
  (modalka-define-kbd "6" "C-t 0")
  (modalka-define-kbd "7" "C-t b")
  (modalka-define-kbd "8" "C-t e")
  (modalka-define-kbd "9" "C-t (")
  ;; (modalka-define-kbd "[" "M-a")
  ;; (modalka-define-kbd "]" "M-e")
  (modalka-define-kbd "'" "C-t C-;")
  (modalka-define-kbd "," "C-t C-t")
  (modalka-define-kbd "." "M-t")
  (modalka-define-kbd "/" "C-/")
  (modalka-define-kbd "=" "C-t +")
  (modalka-define-kbd "-" "C-t C-b")
  (modalka-define-kbd "\\" "C-t 8 RET")
  (modalka-define-kbd "RET" "M-RET")
  (modalka-define-kbd "`" "M-`")
  (modalka-define-kbd "SPC" "C-SPC")
  (modalka-global-mode 1))

;;;_ + Parinfer - Automatic lisp-paren adjusting
(use-package parinfer
  :ensure t
  :delight
  :init
  (setq parinfer-extensions '(defaults smart-tab))
  (add-hook 'parinfer-mode-hook (lambda () (electric-pair-local-mode -1)))
  (add-hook 'common-lisp-mode-hook #'parinfer-mode)
  (add-hook 'emacs-lisp-mode-hook #'parinfer-mode)
  (add-hook 'lisp-mode-hook #'parinfer-mode)
  (add-hook 'lisp-interaction-mode-hook #'parinfer-mode)
  (add-hook 'scheme-mode-hook #'parinfer-mode)
  :config
  (define-key parinfer-mode-map (kbd "M-RET") #'parinfer-toggle-mode))

;;;_ + Smex - Ido for M-x
(use-package smex
  :ensure t
  :bind ("M-t" . smex)
  :config
  (setq smex-save-file (locate-user-emacs-file "smex-items"))
  (smex-initialize))

;;;_ + Smart-tabs - Indent with tab, align with space
(use-package smart-tabs-mode
  :ensure t
  :init
  (smart-tabs-insinuate 'java 'python))

;;;_ + Major mode for Clojure programming language
(use-package clojure-mode
  :ensure t
  :delight "Clj"
  :init
  (add-hook 'clojure-mode-hook #'parinfer-mode)
  (add-hook 'clojure-mode-hook #'subword-mode))

;;;_  - Extra syntax highlighting for clojure.
(use-package clojure-mode-extra-font-locking
  :ensure t)

;;;_  - Interact with a Clojure REPL.
(use-package cider
  :ensure t
  :init
  (add-hook 'cider-mode-hook #'turn-on-eldoc-mode)
  :config
  (setq cider-repl-pop-to-buffer-on-connect t
        cider-show-error-buffer t
        cider-auto-select-error-buffer t
        cider-repl-wrap-history t
        cider-prompt-for-symbol nil
        cider-repl-use-pretty-printing t
        nrepl-log-messages nil))

;;;_ + Major mode for fish scripts
(use-package fish-mode
  :ensure t
  :delight "🐠")

;;;_ + Major mode for Erlang programs
(use-package erlang
  :ensure t
  :delight "Erl"
  :init
  (add-hook 'erlang-mode-hook #'aggressive-indent-mode)
  :config
  (define-key erlang-mode-map (kbd "C-c C-c") #'erlang-compile)
  (require 'erlang-flymake)
  (define-skeleton erlang-test-template-skel
    "Make new *_test() Erlang test function."
    nil
    (setq erl-test-skel/fun-name
          (skeleton-read "Function name: "))
    "_test() ->" \n
    > '(erlang-test-case-skel)
    -6 "." \n \n _)
  (define-skeleton erlang-test-case-skel
    "Make new ‘?assertEqual(…),’ test case."
    nil
    ("Success state: " "?assertEqual(" str ", "
     (setq skeleton-subprompt erl-test-skel/fun-name) "("
     (skeleton-read "%s args: ") "))," \n)))

;;;_ + Major mode for Zig programs
(use-package zig-mode
  :ensure t
  :delight "⭍↯")

;;;_ + Major mode for Go programs
(use-package go-mode
  :ensure t
  :delight "五")      ; ゴ

;;;_ + Major mode for Racket programs
(use-package racket-mode
  :ensure t
  :delight "Rkt"
  :init
  (add-hook 'racket-mode-hook #'parinfer-mode))

;;;_ + Major mode for Scribble documents
(use-package scribble-mode
  :ensure t
  :delight " Scrbl")

;;;_ + Major mode for Haskell programs
(use-package haskell-mode
  :ensure t
  :delight "Hλ")

;;;_ + Aggressive-indent - Re-indent source-code as you type
(use-package aggressive-indent
  :ensure t
  :delight " 🔚")

;;;_ + Major mode for Lisp-Flavoured Erlang programs
(use-package lfe-mode
  :ensure t
  :init
  (add-hook 'lfe-mode-hook #'parinfer-mode)
  (add-hook 'lfe-mode-hook #'rainbow-delimiters-mode))

;;;_ + Major mode for Groff and Nroff files
(use-package nroff-mode
  :delight "Grf"
  :init
  (add-hook 'nroff-mode-hook #'flyspell-mode)
  (add-hook 'nroff-mode-hook #'nroff-electric-mode)
  (add-hook 'nroff-mode-hook (lambda () (setq indent-tabs-mode t))))

;;;_ + Focus - Highlight important text-sections
(use-package focus
  :ensure t
  :init
  ;; (add-hook 'nroff-mode-hook #'focus-mode)
  :config
  (add-to-list 'focus-mode-to-thing #'(nroff-mode . paragraph)))

;;;_ + Darkroom - Remove distractions; center text
(use-package darkroom
  :ensure t
  :delight darkroom-tentative-mode " ▓"
  :init)
  ;; (add-hook 'nroff-mode-hook #'darkroom-tentative-mode))

;;;_ + Major mode for Forth programs
(use-package forth-mode
  :ensure t)

;;;_ + Major mode for Rust programs
(use-package rust-mode
  :ensure t
  :delight "Rst")

;;;_ + GGTags - GNU `global' tagging integration
(use-package ggtags
  :ensure t
  :delight " GG")

;;;_ + Paradox - Alternative Emacs package menu
(use-package paradox
  :ensure t
  :init
  (paradox-enable))

;;;_ + Major mode for markdown documents
(use-package markdown-mode
  :ensure t
  :delight "M🠋")

;;;_ + Interface to `w3m' text browser
(use-package w3m
  :ensure t
  :init
  (add-to-list 'modalka-excluded-modes #'w3m)
  :config
  (setq w3m-home-page "https://duckduckgo.com/lite/"))

;;;_ + Interface to `mu' (mu4e)
(use-package mu4e
  :init
  (setq mail-user-agent 'mu4e-user-agent)
  (add-to-list 'modalka-excluded-modes #'mu4e-headers-mode)
  (add-to-list 'modalka-excluded-modes #'mu4e-main-mode)
  (add-to-list 'modalka-excluded-modes #'mu4e-view-mode)
  (add-hook 'mu4e-headers-mode-hook (lambda () (setq-local scroll-margin 4)))
  :config
  (setq mu4e-personal-addresses '("fdriver@posteo.net"
                                  "11fd@posteo.me")
;;;_  - Directories
        mu4e-root-maildir "/home/fd/Maildir"
        mu4e-sent-folder "/Sent"
        mu4e-drafts-folder "/Drafts"
        mu4e-trash-folder "/Trash"
        mu4e-maildir-shortcuts '(("/INBOX"  . ?i)
                                 ("/Sent"   . ?s)
                                 ("/Drafts" . ?d)
                                 ("/Trash"  . ?t))
        mu4e-attachment-dir "~/Elŝutoj"
;;;_  - Look & Feel
        mu4e-headers-date-format "%Y-%m-%d %H:%M"
        mu4e-view-show-addresses t
        message-kill-buffer-on-exit t
        mu4e-html2text-command "w3m -dump -T text/html"
        ;; mu4e-html2text-command 'mu4e-shr2text
        mu4e-get-mail-command "mbsync -a"
        message-send-mail-function 'message-send-mail-with-sendmail
        mu4e-change-filenames-when-moving t ; Prevent UID errors.
        mu4e-confirm-quit nil
;;;_  - Citations
        message-citation-line-format "%N @ %Y-%m-%d %H:%M %Z:\n"
        message-citation-line-function 'message-insert-formatted-citation-line)
;;;_  - Actions
  (add-to-list 'mu4e-headers-actions '("in browser" . mu4e-action-view-in-browser) t)
  (add-to-list 'mu4e-view-actions '("in browser" . mu4e-action-view-in-browser) t))

;;;_ + BBYAC - Complete via acronym
(use-package bbyac
  :ensure t
  :delight " ᵇᶜ"
  :init
  (add-hook 'erlang-mode-hook #'turn-on-bbyac-mode)
  (add-hook 'emacs-lisp-mode-hook #'turn-on-bbyac-mode))

;;;_ + Avy - Jump to anywhere
(use-package avy
  :ensure t
  :bind ("C-z" . avy-goto-char-timer)
  :config
  (setq avy-timeout-seconds 0.3))

;;;_ + Tomorrow theme
(use-package color-theme-sanityinc-tomorrow
  :ensure t
  :init
  (setq custom-safe-themes t)
  :config
  (load-theme 'sanityinc-tomorrow-night))

;;;_ + Major mode for Emacs lisp programs
(use-package elisp-mode
  :config
  (define-key emacs-lisp-mode-map (kbd "C-c C-c") 'emacs-lisp-byte-compile-and-load))

;;;_ + Major mode for TeX/LaTeX/XeTeX/ConTeXt/SILE documents
(use-package tex
  :ensure auctex
  :config
  (setq TeX-view-program-selection
        '(((output-dvi has-no-display-manager) "dvi2tty")
          ((output-dvi style-pstricks) "dvips and gv")
          (output-dvi "xdvi")
          (output-pdf "Zathura")
          (output-html "xdg-open"))))

;;;_ + Search with ripgrep (rg)
(use-package deadgrep
  :ensure t)

;;;_ + Major mode for AsciiDoc documents
(use-package adoc-mode
  :ensure t)
