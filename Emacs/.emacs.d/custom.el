(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   '(zig-mode which-key w3m use-package smex smart-tabs-mode scribble-mode rust-mode rainbow-delimiters racket-mode parinfer paradox modalka markdown-mode lfe-mode haskell-mode go-mode ggtags forth-mode focus fish-mode erlang delight deadgrep darkroom color-theme-sanityinc-tomorrow clojure-mode-extra-font-locking cider bbyac avy auctex aggressive-indent adoc-mode)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
